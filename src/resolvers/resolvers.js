const db = require('../utils/db')
const shortid = require('shortid')

export const resolvers = {
  Query: {
    getAllProducts: (_parent, _args, _context, _info) => db.collection('products').find({}).toArray(),
    getProductById: (parent, args, _context, _info) => {
      const productId = args._id
      return db.collection('products').findOne({ _id: productId })
    },
    getProductsByProducer: (_parent, args, _context, _info) => {
      const { producerID } = args
      return db.collection('products').find({ producerID: producerID }).toArray()
    },
    getProductsByColor: (_parent, args, _context, _info) => {
      const { color } = args
      return db.collection('products').find({ color: color }).toArray()
    },
    getProductsByCategory: (_parent, args, _context, _info) => {
      const { category } = args
      return db.collection('products').find({ category: category }).toArray()
    },
    getProductsByBrand: (_parent, args, _context, _info) => {
      const { brand } = args
      return db.collection('products').find({ brand: brand }).toArray()
    },
    getProductsByName: (_parent, args, _context, _info) => {
      const { name } = args
      return db.collection('products').find({ name: {$regex: `.*${name}.*`, $options: 'i'} }).toArray()
    }
  },
  Mutation: {
    createProduct: async (_parent, args, context, _info) => {
      const { createProductInput } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == createProductInput.producerID || user.accountType == 'ADMIN') {
        createProductInput._id = shortid.generate()
        createProductInput.createdAt = new Date()
        const insertedProduct = await db.collection('products').insertOne(createProductInput)
        return insertedProduct.ops[0]
      } else {
        return null
      }
    },
    updateProduct: async (_parent, args, context, _info) => {
      const { producerID, _id, updateProductInput } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == producerID || user.accountType == 'ADMIN') {
        await db.collection('products').findOneAndUpdate({ _id: _id }, { $set: updateProductInput })
        return db.collection('products').findOne({ _id: _id })
      } else {
        return null
      }
    },
    deleteProduct: async (_parent, args, context, _info) => {
      const { producerID, _id } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == producerID || user.accountType == 'ADMIN') {
        await db.collection('products').deleteOne({ _id: _id })
        return null
      } else {
        return null
      }
    },
    deleteProductsByProducer: async (_parent, args, context, _info) => {
      const { producerID } = args
      await db.collection('products').deleteMany({ producerID })
    }
  },
}
