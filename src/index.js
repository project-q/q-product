import { ApolloServer, gql, PubSub } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import { resolvers } from './resolvers/resolvers';
import config from 'config';
const fs = require('fs')
const path = require('path')
const db = require('./utils/db');

const types = fs.readFileSync(path.join(__dirname, './schemas/schemas.graphql'), 'utf8')

const port = process.env.SERVICE_PORT || 3000;
const url = `mongodb://${process.env.MONGO_USER || 'root'}:${process.env.MONGO_PASSWORD || 'example'}@${process.env.MONGO_SERVER || 'localhost'}:27017`

const mongodb = config.get('mongoURL');

const pubsub = new PubSub()

const schema = buildFederatedSchema([{ typeDefs: gql`${types}`, resolvers }])

const server = new ApolloServer({
  schema,
  context: req => ({
    ...req,
    pubsub,
  }),
  playground: {
    endpoint: '/graphql',
  },
})

db.connect(url, err => {
  if(err){
    console.log('Unable to connect to database')
  }else{
    server
      .listen({port: port})
      .then(({url}) => {
        console.log(`connected to mongoDB`)
        console.log(`Product-app is running at localhost: ${url}`)
      })
  }
})
